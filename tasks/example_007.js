// Реализовать функцию indexOfAll.
// Первый аргумент - массив, второй - значение

// Функция возвращает массив со всеми индексами, которые соответствуют переданному значению

const indexOfAll = (arr, findNum) => {
  return arr.reduce(
    (indexArray, currNum, ind) =>
      currNum === findNum ? [...indexArray, ind] : indexArray,
    []
  );
};

export { indexOfAll };
