// Каррирование
// add(4)(3)(1) => 8

const add = (a = 0) => {
  let currentSum = a;
  const closingFunc = (b = 0) => {
    currentSum += b;
    return closingFunc;
  };

  closingFunc.toString = () => currentSum;
  closingFunc.valueOf = () => currentSum;

  return closingFunc;
};

export { add };
