// Дан массив чисел. Необходимо отсортировать нечетные числа
// в порядке возрастания, оставив четные числа на их исходных позициях.

const sortOddsOnly = (arr) => {
  const oddsArray = arr
    .filter((num) => num % 2)
    .sort((curr, next) => curr - next);

  return arr.map((num) => (num % 2 ? oddsArray.shift() : num));
};

export { sortOddsOnly };
