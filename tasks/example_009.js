// const factory = (xValue, yValue, funcSumName) => {
//     ...
//   }
//   const obj = factory(12, 23, 'myFunc');

//   console.log(obj.x, obj.y, obj.myFunc()); // 12, 23, 35

const factory = (x, y, funcName) => {
  return new (function () {
    this.x = x;
    this.y = y;
    this[funcName] = () => this.x + this.y;
  })();
};

export { factory };
