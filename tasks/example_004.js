// Функция принимает 2 массива, и возвращает массив объединенных значений,
// без дублирования

const union = (arr1, arr2) => {
  return [...new Set(arr1.concat(arr2))];
};

export { union };
