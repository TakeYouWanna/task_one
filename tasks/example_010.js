// const obj = {
//     id: 0,
//     name: 'Obj-name',
//     // ...
//   };

//   console.log(`Name: ${obj}`); 		// Name: Obj-name
//   console.log(+obj);            		// 0
//   console.log(obj + 10);             //10

function toysObj(id, name) {
    this.id = id;
    this.name = name;
    this.toString = function () {
        return this.name;
    };
    this.valueOf = function () {
        return this.id;
    };
}

export { toysObj };
