// Создать функцию с именем divisors / Divisors, которая принимает целое число n> 1
// и возвращает массив со всеми делителями целого числа (кроме 1 и самого числа), от наименьшего до наибольшего.
// Если число простое, вернуть строку '(integer) is prime'.

const divisors = (num) => {
  let divisorArray = [];
  let i = 2;
  while (i < num) {
    if (num % i === 0) divisorArray = [...divisorArray, i];
    i++;
  }
  return divisorArray;
};

export { divisors };
