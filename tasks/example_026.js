// Вернуть средний символ слова. Если длина слова нечетная, вернуть средний символ.
//  Если длина слова четная, вернуть 2 средних символа.

const getMiddle = (str) =>
  str.length % 2
    ? str.substr((str.length - 1) / 2, 1)
    : str.substr(str.length / 2 - 1, 2);

export { getMiddle };
