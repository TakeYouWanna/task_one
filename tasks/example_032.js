// Создать функцию, которая принимает римское число в качестве аргумента
// и возвращает его значение как числовое десятичное целое число. Не нужно проверять форму римской цифры.
// Римские цифры записываются путем выражения каждой десятичной цифры числа,
// которые нужно кодировать отдельно, начиная с крайней левой цифры. Таким образом, 1990 год
// отображается как «MCMXC» (1000 = M, 900 = CM, 90 = XC), а 2008 год отображается как «MMVIII»
// (2000 = MM, 8 = VIII). В римской числе 1666 года «MDCLXVI» каждая буква используется в порядке убывания.
// Пример:
// solution('XXI'); 	// 21
// Помoщь:
// Symbol	Value
// I          	1
// V          	5
// X          	10
// L          	50
// C          	100
// D		    500
// M          	1,000

const solution = (romanNumber) => {
    const arabicParser = { M: 1000, D: 500, C: 100, L: 50, X: 10, V: 5, I: 1 };
    return romanNumber
        .toUpperCase()
        .split('')
        .reduce(
            (arabicNumber, currRomanNum, index, romanNumbers) =>
                arabicNumber +
                (arabicParser[currRomanNum] <
                arabicParser[romanNumbers[index + 1]]
                    ? -arabicParser[currRomanNum]
                    : arabicParser[currRomanNum]),
            0
        );
};

export { solution };
