// Реализовать функцию forEachRight
// Первый аргумент - массив, второй - функция, применяется на массив в обратном порядке

const forEachRight = (arr, callback = null) =>
  arr.reverse().map((elem) => callback(elem));

export { forEachRight };
