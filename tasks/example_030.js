// Написать функцию, которая принимает строковый параметр и
// переворачивает каждое слово в строке. Все пробелы в строке следует сохранить.

const wordsReverse = (str) =>
  str
    .split(' ')
    .map((word) => word.split('').reverse().join(''))
    .join(' ');

export { wordsReverse };
