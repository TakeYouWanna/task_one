// Написать функцию, которая возвращает минимальное
// и максимальное количество заданного списка / массива.

const minMax = (arr) => Array.of(Math.min(...arr), Math.max(...arr));

export { minMax };
