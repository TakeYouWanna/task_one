// camelize("background-color") == 'backgroundColor';
// camelize("list-style-image") == 'listStyleImage';
// camelize("-webkit-transition") == 'WebkitTransition';

const camelCase = (str) => {
  return str
    .replace(/(?:^\w|\b\w)/g, (word, index) =>
      index ? word.toUpperCase() : word.toLowerCase()
    )
    .replace(/-/g, '');
};

export { camelCase };
