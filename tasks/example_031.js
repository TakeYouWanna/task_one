// Реализовать функцию uniqueInOrder, которая принимает в качестве
// аргумента последовательность и возвращает список элементов без каких-либо
// элементов с одинаковым значением рядом друг с другом и с сохранением исходного порядка элементов.

const uniqueInOrder = (sequence) =>
  Array.from(sequence).reduce(
    (uniqueSequence, currItem) =>
      uniqueSequence[uniqueSequence.length - 1] === currItem
        ? uniqueSequence
        : [...uniqueSequence, currItem],
    []
  );

export { uniqueInOrder };
