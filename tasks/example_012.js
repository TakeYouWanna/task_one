// На входе массив чисел, например: arr = [1, -2, 3, 4, -9, 6].

// Задача: найти непрерывный подмассив в arr, сумма элементов в котором максимальна.

// Функция getMaxSubSum(arr) должна возвращать эту сумму.

const getMaxSubSum = (arr) => {
  let maxSum = 0;
  arr.reduce((currSum, currVal) => {
    currSum += currVal;
    maxSum = Math.max(maxSum, currSum);
    if (currSum < 0) currSum = 0;
    return currSum;
  }, 0);
  return maxSum;
};

export { getMaxSubSum };
