// Напишите функцию sumTo(n), которая вычисляет сумму чисел 1 + 2 + ... + n.

const sumTo = (n) => (n !== 0 ? n + sumTo(n - 1) : n);

export { sumTo };
