// Напишите функцию, которая принимает строку из одного или нескольких
// слов и возвращает ту же строку, но с перевернутыми словами из X и
// более букв (X — второй параметр ф-ии).. Переданные строки будут состоять только из букв и пробелов.

const spinWords = (str, wordLength) => {
  let delimiter = ' ';
  let words = str.split(delimiter);
  return words
    .map((word) =>
      word.length >= wordLength ? word.split('').reverse().join('') : word
    )
    .join(delimiter);
};

export { spinWords };
