// Реализовать функцию createGenerator.
// При каждом вызове метода .next() происходит возврат следующего значения из массива
// Когда все элементы будут возвращены,
// следующие вызовы метода .next() должны возвращать строку 'Complete!'

function* createGenerator(arr) {
  while (arr.length) yield console.log(arr.shift());
  while (true) yield console.log('Complete!');
}

export { createGenerator };
