// Написать функцию, которая будет возвращать количество отдельных
// нечувствительных к регистру буквенных символов и числовых цифр,
// которые встречаются во входной строке более одного раза. Предполагается,
// что входная строка содержит только буквы (как в верхнем, так и в нижнем регистре) и цифры.

const numberRepeatedCharacters = (str) => {
  let lowerCharArray = str.toLowerCase().split('');
  let uniqueCharSet = new Set();
  while (lowerCharArray.length) {
    let currentChar = lowerCharArray.shift();
    if (lowerCharArray.includes(currentChar)) uniqueCharSet.add(currentChar);
  }
  return uniqueCharSet.size;
};

export { numberRepeatedCharacters };
