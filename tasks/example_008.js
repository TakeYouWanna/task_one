// Функция принимает массив meetups,
// и возвращает суммарное количество человек, находящихся на активных митапах

// membersOnActiveMeetups(meetups); 1500

const membersOnActiveMeetups = (arr) =>
  arr.reduce(
    (quantity, currMeeting) =>
      currMeeting.isActive ? (quantity += currMeeting.members) : quantity,
    0
  );

export { membersOnActiveMeetups };
