// Функция принимает 2 массива.
// Возвращает новый массив, который состоит только из тех элементов,
// которые встретились в одном массиве, но не встретились в другом

const arrayDiff = (arr1, arr2) => {
  const filteredArray = arr1.concat(arr2);
  return filteredArray.filter((el) => !arr1.includes(el) || !arr2.includes(el));
};

export { arrayDiff };
