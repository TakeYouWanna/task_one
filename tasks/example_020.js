// Написать функцию persistence, которая принимает положительный параметр
// num и возвращает число, которое нужно умножить
// цифры в num, пока не дойдете до единственной цифры.

const persistence = (num) => {
  let counter = 0;
  while (num > 10) {
    num = num
      .toString()
      .split('')
      .reduce((multiplied, currNum) => (multiplied *= currNum), 1);
    counter++;
  }
  return counter;
};

export { persistence };
