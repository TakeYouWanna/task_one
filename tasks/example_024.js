// Дана строка. Вернуть длину самого короткого слова

const getLengthOfShortestWord = (str) =>
  str
    .replace(',', '')
    .replace('.', '')
    .split(' ')
    .reduce((prevWord, curWord) => {
      return prevWord.length > curWord.length ? curWord : prevWord;
    }).length;

export { getLengthOfShortestWord };
