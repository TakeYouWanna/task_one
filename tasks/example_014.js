// Напишите функцию filterRange(arr, a, b),
// которая принимает массив arr, ищет в нём элементы между a и b
// и отдаёт массив этих элементов.

const filterRange = (arr, begin, end) => {
  begin = arr.indexOf(begin);
  if (begin < 0) begin = null;
  end = arr.lastIndexOf(end) + 1;
  if (end < 0 || begin === null) end = null;
  return array.slice(begin, end);
};

export { filterRange };
