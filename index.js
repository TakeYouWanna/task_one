import { any } from './tasks/example_001.js';
import { arrayDiff } from './tasks/example_002.js';
import { forEachRight } from './tasks/example_003.js';
import { union } from './tasks/example_004.js';
import { createGenerator } from './tasks/example_005.js';
import { without } from './tasks/example_006.js';
import { indexOfAll } from './tasks/example_007.js';
import { membersOnActiveMeetups } from './tasks/example_008.js';
import { factory } from './tasks/example_009.js';
import { toysObj } from './tasks/example_010.js';
import { add } from './tasks/example_011.js';
import { getMaxSubSum } from './tasks/example_012.js';
import { camelCase } from './tasks/example_013.js';
import { filterRange } from './tasks/example_014.js';
import { unique } from './tasks/example_015.js';
import { sumTo } from './tasks/example_016.js';
import { spinWords } from './tasks/example_017.js';
import { filter_list } from './tasks/example_018.js';
import { squaring } from './tasks/example_019.js';
import { persistence } from './tasks/example_020.js';
import { converter10 } from './tasks/example_021.js';
import { divisors } from './tasks/example_022.js';
import { rowSumOddNumbers } from './tasks/example_023.js';
import { getLengthOfShortestWord } from './tasks/example_024.js';
import { isTriangle } from './tasks/example_025.js';
import { getMiddle } from './tasks/example_026.js';
import { sortOddsOnly } from './tasks/example_027.js';
import { numberRepeatedCharacters } from './tasks/example_028.js';
import { minMax } from './tasks/example_029.js';
import { wordsReverse } from './tasks/example_030.js';
import { uniqueInOrder } from './tasks/example_031.js';
import { solution } from './tasks/example_032.js';

// console.log('example_001.js');
// console.log(any([1, 2, 3, 0], (el) => el > 2));

// console.log('example_002.js');
// console.log(arrayDiff([1, 2, 3, 4], [5, '4', 3, 2, 7]));

// console.log('example_003.js');
// forEachRight([1, 2, 3, 4], (el) => console.log(el));

// console.log('example_004.js');
// console.log(union([1, 2, 3, 4, 5], [7, 6, 5, 4, '4', 8]));

// console.log('example_005.js');
// const generator = createGenerator([1, 2, 4, 'we']);
// generator.next();
// generator.next();
// generator.next();
// generator.next();
// generator.next();
// generator.next();
// generator.next();

// console.log('example_006.js');
// console.log(without([1, 2, 3], 1, 6, 7, 8));

// console.log('example_007.js');
// console.log(indexOfAll([1, 'qwe', 1, 'qwe', 5], 'qwe'));

// console.log('example_008.js');
// const meetups = [
//     { name: 'JavaScript', isActive: true, members: 100 },
//     { name: 'Angular', isActive: true, members: 900 },
//     { name: 'Node', isActive: false, members: 600 },
//     { name: 'React', isActive: true, members: 500 },
// ];
// console.log(membersOnActiveMeetups(meetups));

// console.log('example_009.js');
// const obj = factory(1, 2, 'f');
// console.log(obj.f());
// obj.x = 10;
// console.log(obj.f());

// console.log('example_0010.js');
// const obj = new toysObj(5, 'Obj-name');
// console.log(`Name: ${obj}`);
// console.log(+obj);
// console.log(obj + 10);

// console.log('example_0011.js');
// console.log(add(1)(4)(5).toString());

// console.log('example_0012.js');
// console.log(getMaxSubSum([-100, -9, -2, -3, -5]));

// console.log('example_0013.js');
// console.log(camelCase('Qwe-asd-qwe'));

// console.log('example_0014.js');
// console.log(filterRange([-1, 1, 2, 3, 4, 5, 10], 1, 3));

// console.log('example_0015.js');
// let strings = ['aaa', 'aaa', 'zzz', 'xxx', 'aaa', 'bbb', 'aaa', 'xxx', 'ccc'];
// console.log(unique(strings));

// console.log('example_0016.js');
// console.log(sumTo(5));

// console.log('example_017.js');
// console.log(spinWords('This is another test', 3));

// console.log('example_018.js');
// console.log(filter_list([1, 2, 'aasf', '1', '123', 123]));

// console.log('example_019.js');
// console.log(squaring(-123123));

// console.log('example_020.js');
// console.log(persistence(29));

// console.log('example_021.js');
// console.log(converter10([1, 1, 1, 1]));

console.log('example_022.js');
console.log(divisors(12));

// console.log('example_023.js');
// console.log(rowSumOddNumbers(4));

// console.log('example_024.js');
// console.log(
//     getLengthOfShortestWord(
//         'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.'
//     )
// );

// console.log('example_025.js');
// console.log(isTriangle(1, 2, 10));

// console.log('example_026.js');
// console.log(getMiddle('texts'));

// console.log('example_027.js');
// console.log(sortOddsOnly([9, 8, 7, 6, 5, 4, 3, 2, 1, 0]));

// console.log('example_028.js');
// console.log(numberRepeatedCharacters('qwWfawWwwaW123#'));

// console.log('example_029.js');
// console.log(minMax([1]));

// console.log('example_030.js');
// console.log(wordsReverse('This is an example!'));

// console.log('example_031.js');
// console.log(uniqueInOrder([1, 2, 3, 4, 1, 2, 2, 1, 1, 1, 1, 2, 2, 4, 5]));

console.log('example_032.js');
console.log(solution('MMVIII'));
